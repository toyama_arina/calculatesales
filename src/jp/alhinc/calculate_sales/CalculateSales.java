package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 支店定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	// 追加エラーメッセージ
	private static final String OVER_SALES_ERROR = "合計金額が10桁を超えました";
	private static final String FILE_NAME_NOT_SERIAL = "売上ファイル名が連番になっていません";
	private static final String BRANCH_NOT_EXIST = "の支店コードが不正です";
	private static final String COMMODITY_NOT_EXIST = "の商品コードが不正です";
	private static final String INVALID_FORMAT = "のフォーマットが不正です";

	// 定義名
	private static final String BRANCH = "支店";
	private static final String COMMODITY = "商品";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// エラー処理(3-1)
		// 1. コマンドライン引数が渡されていない場合は、エラーメッセージ「予期せぬエラーが発生しました」を表示し、処理を終了する。
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, BRANCH, "^[0-9]*$")) {
			return;
		}

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, COMMODITY,"^[A-Za-z0-9]{8}$")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		// file一覧を参照
		for(int i = 0; i < files.length; i++){
			// ^[0-9]{8}
			if(files[i].isFile() && files[i].getName().matches("^\\d{8}.rcd$")){
				rcdFiles.add(files[i]);
			}
		}

		// エラー処理（2-1)
		// 売上ファイルが連番になっていない場合、エラーメッセージ「売上ファイル名が連番になっていません」を表示し、処理を終了する。
		// file一覧を昇順にソートする
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println(FILE_NAME_NOT_SERIAL);
				return;
			}
		}

		// 各支店集計ファイルを読み込み、集計する
		for (File file: rcdFiles) {
			BufferedReader br = null;
			try {
				File openFile = new File(args[0], file.getName());
				FileReader fr = new FileReader(openFile);
				br = new BufferedReader(fr);

				String line = "";
				ArrayList<String> rcdData = new ArrayList<String>();
				while((line = br.readLine()) != null) {
					rcdData.add(line);
				}

				// エラー処理(2-4)
				// 4. 売上ファイルの中身が3行以上ある場合は、エラーメッセージ「<該当ファイル名>のフォーマットが不正です」と表示し処理を終了する。
				// 商品定義の追加(4)
				// 商品の追加に伴い、3桁以外はエラーとする
				if(rcdData.size() != 3) {
					System.out.println(file.getName() + INVALID_FORMAT);
					return;
				}

				String branchCode = rcdData.get(0);
				String commodityCode = rcdData.get(1);
				String branch_commodity_Sales = rcdData.get(2);
				// エラー処理(2-3)
				// 3. 支店に該当がなかった場合は、エラーメッセージ「<該当ファイル名>の支店コードが不正です」と表示し、処理を終了する。
				if (!branchNames.containsKey(branchCode)) {
					System.out.println(file.getName() + BRANCH_NOT_EXIST);
					return;
				// 商品コードに該当がなかった場合は、エラーとなる。
				}
				if(!commodityNames.containsKey(commodityCode)) {
					System.out.println(file.getName() + COMMODITY_NOT_EXIST);
					return;
				}

				// エラー処理(3-2)
				// 2. 売上ファイルの売上金額が数字ではなかった場合は、エラーメッセージ「予期せぬエラーが発生しました」を表示し、処理を終了する。
				if(!branch_commodity_Sales.matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				// エラー処理(2-2)
				// 2. 合計金額が10桁を超えた場合、エラーメッセージ「合計金額が10桁を超えました」を表示し、処理を終了する。
				long fileSale = Long.parseLong(branch_commodity_Sales);
				Long branchSaleAmount = branchSales.get(branchCode) + fileSale;
				Long commoditySaleAmount = commoditySales.get(commodityCode) + fileSale;
				if((branchSaleAmount >= 10000000000L) || (commoditySaleAmount >= 10000000000L)){
					// 売上金額が11桁以上の場合、エラーメッセージをコンソールに表示します。
					System.out.println(OVER_SALES_ERROR);
					return;
				}
				branchSales.put(branchCode, branchSaleAmount);
				commoditySales.put(commodityCode, commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義・商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 各コードと各名称を保持するMap
	 * @param 各コードと各売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> allNames, Map<String, Long> allSales, String definition, String regexp) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			// エラー処理(1-1)
			// 支店定義ファイルが存在しない場合は、エラーメッセージ「支店定義ファイルが存在しません」を表示し、処理を終了する。
			File checkFile = new File(path, fileName);
			if((!checkFile.exists())){
				System.out.println(definition + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] readData;
				readData = line.split(",");

				// エラー処理（1-2）
				//  支店定義ファイルのフォーマットが不正な場合は、エラーメッセージ「支店定義ファイルのフォーマットが不正です」を表示し、処理を終了する。
				if((readData.length != 2) || (!readData[0].matches(regexp))) {
					System.out.println(definition +  FILE_INVALID_FORMAT);
					return false;
				}
				allNames.put(readData[0], readData[1]);
				allSales.put(readData[0], (long)0);
			}

		} catch(Exception e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル・商品別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コード（商品コード）と支店名（商品名）を保持するMap
	 * @param 支店コード（商品コード）と売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> readNames, Map<String, Long> readSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		File file = new File(path, fileName);

		BufferedWriter bw = null;

		try {
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key:  readNames.keySet()) {
				// 入力形式
				// 支店コード,支店名,支店別合計
				// 001,札幌支店,922000
				bw.write(key + "," + readNames.get(key) + "," + readSales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;

	}


}
